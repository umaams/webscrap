'use strict'

const req = require('request')
var rp = require('request-promise')
const cheerio = require('cheerio')
class SortitusiController {

	async facepack ({request, response}) {
		let arrimgurl = []
		let $ = null
		const url = 'https://sortitoutsi.net/football-manager-2018/team/1664/a-bilbao'
		await rp(url).then(function (body) {
		  $ = cheerio.load(body)
		}).then(function(e) {
		  $('table.table-striped tbody tr').each((i, value) => {
		  	arrimgurl.push({
		  		name: $(value).find('td').eq(1).find('a').first().attr('title'),
		  		url: $(value).find('td').eq(0).find('img').attr('src').replace('iconface', 'face'),
		  		nationality: $(value).find('td').eq(1).find('.item_info').find('a').first().attr('title'),
		  		position: $(value).find('td').eq(3).text().replace('\n                    ', '').replace('\n                ', ''),
		  		rating: $(value).find('td').eq(8).attr('value')
		  	})
		  })
		})
		response.status(200).json(arrimgurl)
	}
}

module.exports = SortitusiController
